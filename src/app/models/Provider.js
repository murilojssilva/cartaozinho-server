import mongoose from "mongoose";
import { isEmail } from "validator";
const PointSchema = require("./utils/point.js");

const Provider = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    specialty: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: [isEmail, "E-mail inválido"],
      createIndexes: { unique: true },
    },
    avatar: {
      type: String,
    },
    path: {
      type: String,
    },
    uf: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    avatar: {
      type: String,
    },
    description: {
      type: String,
    },
    twitter: {
      type: String,
    },
    instagram: {
      type: String,
    },
    neighborhood: {
      type: String,
    },
    website: {
      type: String,
    },
    type: {
      type: String,
      required: true,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
    updatedAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    timestemps: true,
  }
);

module.exports = mongoose.model("Provider", Provider);
