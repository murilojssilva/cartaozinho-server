import mongoose from "mongoose";
import bcrypt from "bcryptjs";
import { isEmail } from "validator";

const User = new mongoose.Schema(
  {
    first_name: {
      type: String,
      required: true,
    },
    last_name: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: [isEmail, "E-mail inválido"],
      createIndexes: { unique: true },
    },
    avatar: {
      type: String,
    },
    path: {
      type: String,
    },
    password: { type: String, required: true },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    updatedAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    timestemps: true,
  }
);

User.pre("save", async function (next) {
  let user = this;
  if (!user.isModified("password")) return next();
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;
  next();
});

User.methods = {
  comparePassword(pass) {
    return bcrypt.compare(pass, this.password);
  },
};

module.exports = mongoose.model("User", User);
