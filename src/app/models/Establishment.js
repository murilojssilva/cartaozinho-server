import mongoose from "mongoose";
import { isEmail } from "validator";

const Establishment = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: [isEmail, "E-mail inválido"],
      createIndexes: { unique: true },
    },
    avatar: {
      type: String,
    },
    description: {
      type: String,
    },
    avatar: {
      type: String,
    },
    path: {
      type: String,
    },
    password: { type: String, required: true },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    updatedAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    timestemps: true,
  }
);

module.exports = mongoose.model("Establishment", Establishment);
