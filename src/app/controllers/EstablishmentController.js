import * as Yup from "yup";
import mongoose from "mongoose";
import "../models/Establishment";

const Establishment = mongoose.model("Establishment");

class EstablishmentController {
  async show(request, response) {
    const { id } = request.params;
    const establishment = await Establishment.findById(id);
    if (!establishment) {
      return response.status(400).json({ message: "Establishment not found" });
    }
    return response.json({ establishment });
  }
  async index(request, response) {
    Establishment.find({})
      .then((establishment) => {
        return response.json(establishment);
      })
      .catch((err) => {
        return response.status(400).json({
          error: true,
          message: "Nenhum estabelecimento encontrado.",
        });
      });
  }
  async create(request, response) {
    const scbema = Yup.object().shape({
      name: Yup.string().required(),
      address: Yup.string().required(),
      phone: Yup.string().required(),
      email: Yup.string().email().required(),
      password: Yup.string().required().min(6),
    });
    if (!(await scbema.isValid(request.body))) {
      return response.status(400).json({ error: "Validation fails" });
    }
    const userExists = await Establishment.findOne({
      where: { email: request.body.email },
    });
    if (userExists) {
      return response.status(400).json({ error: "Estabelecimento já existe." });
    }
    const {
      first_name,
      last_name,
      phone,
      email,
      password,
      address,
    } = request.body;
    const { key } = request.file ? request.file : "";
    let url = "";
    if (key) url = `${process.env.APP_URL}files/${key}`;
    else url = "";

    const establishment = Establishment.create(
      {
        first_name,
        avatar: url,
        last_name,
        phone,
        address,
        email,
        password,
      },
      (err) => {
        if (err) {
          return response.status(400).json({
            error: true,
            message: `Erro. Estabelecimento não foi cadastrado. ${err}`,
          });
        }
        return response.status(200).json({
          error: false,
          message: "Estabelecimento foi cadastrado com sucesso.",
        });
      }
    );
  }
  async update(request, response, next) {
    try {
      const schema = Yup.object().shape({
        first_name: Yup.string(),
        last_name: Yup.string(),
        username: Yup.string(),
        phone: Yup.string(),
        city: Yup.string(),
        email: Yup.string().email(),
        uf: Yup.string(),
        oldPassword: Yup.string().min(6),
        password: Yup.string()
          .min(6)
          .when("oldPassword", (oldPassword, field) =>
            oldPassword ? field.required() : field
          ),
        confirmPassword: Yup.string().when("password", (password, field) =>
          password ? field.required().oneOf([Yup.ref("password")]) : field
        ),
      });

      if (!(await schema.isValid(request.body))) {
        return response.status(400).json({ error: "Validation fails" });
      }
      const { email, oldPassword } = request.body;
      const establishment = await Establishment.findById(request.userId);
      if (email !== establishment.email) {
        const userExists = await Establishment.findOne({
          where: { email },
        });
        if (userExists) {
          return response
            .status(400)
            .json({ error: "Estabelecimento já cadastrado." });
        }
      }
      if (oldPassword && (await oldPassword) !== establishment.password) {
        establishment.comparePassword(oldPassword, (err, isMatch) => {
          if (err) throw err;
          if (!isMatch) {
            return response
              .status(401)
              .json({ error: "Senha incorreta. Tente novamente." });
          }
        });
      }
      const { id, name } = await establishment.updateOne(request.body);
      return response.json({ id, name, email, address });
    } catch (err) {
      response.json({ error: `Não foi possível realizar o cadastro. ${err}` });
    }
  }
  async delete(request, response) {
    try {
      Establishment.deleteOne({ _id: request.params.id }).then(() => {
        response
          .status(200)
          .json({
            error: false,
            message: "Estabelecimento deletado com sucesso",
          })
          .catch((err) => {
            response.status(400).json({
              error: true,
              message: "Falha ao deletar estabelecimento",
            });
          });
      });
    } catch (err) {
      console.log(err);
    }
  }
  async showPhoto(request, response) {
    try {
      Establishment.findOne({ originalName: request.params.id })
        .then((post) => {
          response.send(post.imageKey);
        })
        .catch((err) => {
          return response.status(400).json({
            error: true,
            message: "Nenhum foto encontrada.",
          });
        });
    } catch (err) {
      console.log(err);
    }
  }
}

export default new EstablishmentController();
