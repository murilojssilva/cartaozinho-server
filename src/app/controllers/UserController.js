import * as Yup from "yup";
import mongoose from "mongoose";
import "../models/User";

const User = mongoose.model("User");

class UserController {
  async show(request, response) {
    const { id } = request.params;
    const user = await User.findById(id);
    if (!user) {
      return response.status(400).json({ message: "User not found" });
    }
    return response.json({ user });
  }
  async index(request, response) {
    User.find({})
      .then((user) => {
        return response.json(user);
      })
      .catch((err) => {
        return response.status(400).json({
          error: true,
          message: "Nenhum usuário encontrado.",
        });
      });
  }
  async create(request, response) {
    const scbema = Yup.object().shape({
      first_name: Yup.string().required(),
      last_name: Yup.string().required(),
      phone: Yup.string().required(),
      email: Yup.string().email().required(),
      password: Yup.string().required().min(6),
    });
    if (!(await scbema.isValid(request.body))) {
      return response.status(400).json({ error: "Validation fails" });
    }
    const userExists = await User.findOne({
      where: { email: request.body.email },
    });
    if (userExists) {
      return response.status(400).json({ error: "Usuário já existe." });
    }
    const { first_name, last_name, whatsapp, email, password } = request.body;
    const { key } = request.file ? request.file : "";
    let url = "";
    if (key) url = `${process.env.APP_URL}files/${key}`;
    else url = "";
    const user = User.create(
      {
        first_name,
        last_name,
        whatsapp,
        email,
        password,
        avatar: url,
      },
      (err) => {
        if (err) {
          return response.status(400).json({
            error: true,
            message: `Erro. Usuário não foi cadastrado. ${err}`,
          });
        }
        return response.status(200).json({
          error: false,
          message: "Usuário foi cadastrado com sucesso.",
        });
      }
    );
  }
  async update(request, response, next) {
    try {
      const schema = Yup.object().shape({
        first_name: Yup.string(),
        last_name: Yup.string(),
        phone: Yup.string(),
        email: Yup.string().email(),
        oldPassword: Yup.string().min(6),
        password: Yup.string()
          .min(6)
          .when("oldPassword", (oldPassword, field) =>
            oldPassword ? field.required() : field
          ),
        confirmPassword: Yup.string().when("password", (password, field) =>
          password ? field.required().oneOf([Yup.ref("password")]) : field
        ),
      });

      if (!(await schema.isValid(request.body))) {
        return response.status(400).json({ error: "Validation fails" });
      }
      const { email, oldPassword } = request.body;
      const user = await User.findById(request.userId);
      if (email !== user.email) {
        const userExists = await User.findOne({
          where: { email },
        });
        if (userExists) {
          return response.status(400).json({ error: "Usuário já cadastrado." });
        }
      }
      if (oldPassword && (await oldPassword) !== user.password) {
        user.comparePassword(oldPassword, (err, isMatch) => {
          if (err) throw err;
          if (!isMatch) {
            return response
              .status(401)
              .json({ error: "Senha incorreta. Tente novamente." });
          }
        });
      }
      const { id, first_name } = await user.updateOne(request.body);
      return response.json({ id, first_name, email });
    } catch (err) {
      response.json({ error: `Não foi possível realizar o cadastro. ${err}` });
    }
  }
  async delete(request, response) {
    try {
      User.deleteOne({ _id: request.params.id }).then(() => {
        response
          .status(200)
          .json({
            error: false,
            message: "Usuário deletado com sucesso",
          })
          .catch((err) => {
            response.status(400).json({
              error: true,
              message: "Falha ao deletar usuário",
            });
          });
      });
    } catch (err) {
      console.log(err);
    }
  }
  async showPhoto(request, response) {
    try {
      User.findOne({ originalName: request.params.id })
        .then((post) => {
          response.send(post.imageKey);
        })
        .catch((err) => {
          return response.status(400).json({
            error: true,
            message: "Nenhum foto encontrada.",
          });
        });
    } catch (err) {
      console.log(err);
    }
  }
}

export default new UserController();
