import jwt from "jsonwebtoken";
import User from "../models/User";
import * as Yup from "yup";
import authConfig from "../../config/auth";

class SessionController {
  async store(request, response) {
    const schema = Yup.object().shape({
      email: Yup.string().email(),
      username: Yup.string(),
      password: Yup.string().required(),
    });
    if (!(await schema.isValid(request.body))) {
      return response.status(400).json({ error: "Validation fails" });
    }
    let username = request.body.username,
      password = request.body.password,
      email = request.body.email;
    let conditions = !!username ? { username } : { email };
    const user = await User.findOne(conditions);
    if (!user) {
      return response.status(400).json({ error: "User not found" });
    }
    if (!(await user.comparePassword(password))) {
      return response.status(400).json({
        error: "Invalid password",
      });
    }
    const { id, first_name } = user;
    return response.json({
      user: {
        id,
        first_name,
        conditions,
      },
      token: jwt.sign({ id }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    });
  }
}

export default new SessionController();
