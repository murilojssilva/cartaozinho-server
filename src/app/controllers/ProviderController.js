import * as Yup from "yup";
import mongoose from "mongoose";
import "../models/Provider";
import fs from "fs";

const Provider = mongoose.model("Provider");

class ProviderController {
  async show(request, response) {
    const { id } = request.params;
    const provider = await Provider.findById(id);
    if (!provider) {
      return response.status(400).json({ message: "Provider not found" });
    }
    return response.json({ provider });
  }
  async index(request, response) {
    Provider.find({})
      .then((provider) => {
        return response.json(provider);
      })
      .catch((err) => {
        return response.status(400).json({
          error: true,
          message: "Nenhum prestador de serviço encontrado.",
        });
      });
  }
  async create(request, response) {
    const scbema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      specialty: Yup.string().required(),
      description: Yup.string(),
      twitter: Yup.string(),
      neighborhood: Yup.string(),
      instagram: Yup.string(),
      website: Yup.string(),
      ufSelect: Yup.string(2).required(),
      citySelect: Yup.string().required(),
      phone: Yup.string().required(),
      latitude: Yup.number(),
      longitude: Yup.number(),
      type: Yup.string(),
    });
    if (!(await scbema.isValid(request.body))) {
      return response.status(400).json({ error: "Validation fails" });
    }
    const {
      name,
      email,
      specialty,
      description,
      twitter,
      instagram,
      website,
      neighborhood,
      ufSelect,
      citySelect,
      phone,
      latitude,
      longitude,
      type,
      avatarUri,
    } = request.body;
    let url = "";
    const date = Date.now();
    if (avatarUri) {
      let base64Image = avatarUri.split(";base64,").pop();
      url = `tmp/uploads/${date}.png`;
      fs.writeFile(url, base64Image, { encoding: "base64" }, function (err) {
        console.log("File created");
      });
    }
    const provider = Provider.create(
      {
        name,
        email,
        twitter,
        instagram,
        website,
        neighborhood,
        specialty,
        description,
        uf: ufSelect,
        city: citySelect,
        phone,
        avatar: `${date}.png`,
        type,
        location: {
          type: "Point",
          coordinates: [longitude, latitude],
        },
      },
      (err) => {
        if (err) {
          return response.status(400).json({
            error: true,
            message: `Erro. Prestador de serviço não foi cadastrado. ${err}`,
          });
        }
        return response.status(200).json({
          error: false,
          message: "Prestador de serviço foi cadastrado com sucesso.",
        });
      }
    );
    return response.json(provider);
  }
  async update(request, response, next) {
    try {
      const schema = Yup.object().shape({
        first_name: Yup.string(),
        last_name: Yup.string(),
        phone: Yup.string(),
        uf: Yup.string(),
        city: Yup.string(),
        description: Yup.string(),
        twitter: Yup.string(),
        instagram: Yup.string(),
        website: Yup.string(),
        neighborhood: Yup.string(),
        type: Yup.string(),
        location: Yup.string(),
        specialty: Yup.string(),
        email: Yup.string().email(),
        oldPassword: Yup.string().min(6),
        password: Yup.string()
          .min(6)
          .when("oldPassword", (oldPassword, field) =>
            oldPassword ? field.required() : field
          ),
        confirmPassword: Yup.string().when("password", (password, field) =>
          password ? field.required().oneOf([Yup.ref("password")]) : field
        ),
      });

      if (!(await schema.isValid(request.body))) {
        return response.status(400).json({ error: "Validation fails" });
      }
      const { email, oldPassword } = request.body;
      const provider = await Provider.findById(request.userId);
      if (email !== provider.email) {
        const userExists = await Provider.findOne({
          where: { email },
        });
        if (userExists) {
          return response
            .status(400)
            .json({ error: "Prestador de serviço já cadastrado." });
        }
      }
      if (oldPassword && (await oldPassword) !== provider.password) {
        provider.comparePassword(oldPassword, (err, isMatch) => {
          if (err) throw err;
          if (!isMatch) {
            return response
              .status(401)
              .json({ error: "Senha incorreta. Tente novamente." });
          }
        });
      }
      const { id, name } = await provider.updateOne(request.body);
      return response.json({ id, name, email });
    } catch (err) {
      response.json({ error: `Não foi possível realizar o cadastro. ${err}` });
    }
  }
  async delete(request, response) {
    try {
      Provider.deleteOne({ _id: request.params.id }).then(() => {
        response
          .status(200)
          .json({
            error: false,
            message: "Prestador de serviço deletado com sucesso",
          })
          .catch((err) => {
            response.status(400).json({
              error: true,
              message: "Falha ao deletar prestador de serviço",
            });
          });
      });
    } catch (err) {
      console.log(err);
    }
  }
  async showPhoto(request, response) {
    try {
      Provider.findOne({ originalName: request.params.avatar })
        .then((post) => {
          response.send(post.imageKey);
        })
        .catch((err) => {
          return response.status(400).json({
            error: true,
            message: "Nenhuma foto encontrada.",
          });
        });
    } catch (err) {
      console.log(err);
    }
  }
}

export default new ProviderController();
