import app from "./app";
import jwt from "jsonwebtoken";

const PORT = process.env.PORT || 3001;

const server = app.listen(PORT, () => {
  console.log(`Server listen on port: ${PORT}`);
});
