import express from "express";
import routes from "./routes";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import database from "./database";
import io from "socket.io";
import http from "http";
import cors from "cors";
import logConfig from "./config/logConfig";
import path from "path";

class App {
  constructor() {
    this.app = express();
    this.server = http.Server(this.app);
    this.middlewares();
    this.routes();
    this.socket();
    this.error();
    this.connectedUsers = {};
    this.connectedProfessors = {};
  }
  middlewares() {
    this.app.use(express.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    this.app.use(
      "/files",
      express.static(path.resolve(__dirname, "..", "tmp", "uploads"))
    );
    this.app.use(cookieParser());
    this.app.use((request, response, next) => {
      request.io = this.io;
      request.connectedUsers = this.connectedUsers;
      request.connectedProfessors = this.connectedProfessors;

      next();
    });
    this.app.use(cors());
    this.app.use(logConfig.getLogger());
  }
  socket() {
    this.io = io(this.server);
    this.io.on("connection", (socket) => {
      const { user_id } = socket.handshake.query;
      const { professor_id } = socket.handshake.query;
      this.connectedUsers[user_id] = socket.id;
      this.connectedProfessors[professor_id] = socket.id;

      socket.on("disconnect", () => {
        delete this.connectedUsers[user_id];
        delete this.connectedProfessors[professor_id];
      });
    });
  }
  routes() {
    this.app.use(routes);
  }
  database() {
    this.database.use(database);
  }
  error() {
    this.app.use(logConfig.getErrorLogger());
    this.app.use((err, req, res, next) => {
      return res.status(500).json({
        status: "error",
        message: err.message,
        data: err,
      });
    });
  }
}

export default new App().server;
