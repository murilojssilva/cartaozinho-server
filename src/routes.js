import { Router } from "express";
import multer from "multer";
import multerConfig from "./config/multer";

import UserController from "./app/controllers/UserController";
import EstablishmentController from "./app/controllers/EstablishmentController";
import FileController from "./app/controllers/FileController";
import ProviderController from "./app/controllers/ProviderController";
import SessionController from "./app/controllers/SessionController";

import authMiddleware from "./app/middlewares/auth";

const routes = new Router();
const upload = multer(multerConfig);

routes.get("/", (request, response) => {
  return response.json({ message: "Cartãozinho" });
});
routes.post("/users", upload.single("avatar"), UserController.create);
routes.post("/providers", upload.single("avatar"), ProviderController.create);
routes.post(
  "/establishments",
  upload.single("avatar"),
  EstablishmentController.create
);
routes.post("/sessions", SessionController.store);

//routes.use(authMiddleware);

routes.put("/users", upload.single("avatar"), UserController.update);
routes.get("/users/:id", UserController.show, FileController.showPhoto);
routes.get(
  "/establishments/:id",
  EstablishmentController.show,
  FileController.showPhoto
);
routes.get("/providers/:id", ProviderController.show, FileController.showPhoto);
routes.get("/users/", UserController.index);
routes.get("/providers/", ProviderController.index);
routes.get("/establishments/", EstablishmentController.index);
routes.delete("/users/:id", UserController.delete);
routes.delete("/providers/:id", ProviderController.delete);
routes.delete("/establishments/:id", EstablishmentController.delete);

routes.put("/users/:id", UserController.update);
routes.put("/providers/:id", ProviderController.update);
routes.put("/establishments/:id", EstablishmentController.update);

routes.get("/files/:imageKey", FileController.showPhoto);

export default routes;
