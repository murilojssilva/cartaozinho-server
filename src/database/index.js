import mongoose from "mongoose";
require("dotenv").config();

mongoose
  .connect(process.env.MONGO_URL, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Conexão com o MongoDB realizada com sucesso");
  })
  .catch((err) => {
    console.log("Erro. Conexão com o MongoDB não foi realizada " + err);
  });
mongoose.Promise = global.Promise;

export default mongoose;
